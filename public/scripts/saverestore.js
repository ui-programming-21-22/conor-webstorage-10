const LOGIN = document.forms["login"];

    if (localStorage.getItem("user") != null)
    {
        document.getElementById("f1").style.display = "none";
    }
    else
    {
        document.getElementById("dataDetected").style.display = "none";
    }

function saveUser() //login the user
{
    localStorage.setItem("user", LOGIN["username"].value);
    document.getElementById("register").style.display = "none";
    document.getElementById("showIfRegistered").style.display = "flex";
    document.getElementById("displayName").innerHTML = "Welcome " + localStorage.getItem("user") + "!";
}

function validateRestore() //register the user
{
    document.getElementById("register").style.display = "none";
    document.getElementById("showIfRegistered").style.display = "flex";
    document.getElementById("displayName").innerHTML = "Welcome " + localStorage.getItem("user") + "!";
}

function deleteRestore()
{
    localStorage.removeItem("user");
    document.location.reload();
}
