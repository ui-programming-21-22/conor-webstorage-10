

function drawQuad(p1 = new Vector2(),
p2 = new Vector2(),
p3 = new Vector2(),
p4 = new Vector2(),
colour = "black",
fill = false) {
    ctx.beginPath();
    ctx.strokeStyle = colour;
    ctx.fillStyle = colour;
    ctx.moveTo(p1.x,p1.y);
    ctx.lineTo(p2.x,p2.y);
    ctx.lineTo(p3.x,p3.y);
    ctx.lineTo(p4.x,p4.y);
    ctx.lineTo(p1.x,p1.y);
    if (fill) ctx.fill();
    ctx.stroke();
}

function drawText(text = "Hello World", font = "10px monospace", colour = "Black", position = new Vector2()) {
    ctx.font = font;
    ctx.fillStyle = colour;
    ctx.fillText(text, position.x, position.y);
}

function drawImg(src, position = new Vector2(), size = new Vector2()) {
    let img = new Image();
    img.src = src;
    ctx.drawImage(img, position.x, position.y, size.x, size.y);
}

function drawCircle(position = new Vector2(), radius = 1, colour = "Black", fill = false) {
    ctx.beginPath();
    ctx.strokeStyle = colour;
    ctx.fillStyle = colour;
    ctx.arc(position.x, position.y, radius, 0, 2 * Math.PI);
    if (fill) ctx.fill();
    ctx.stroke();
}

function Vector2(x = 0,y = 0) {
    this.x = x;
    this.y = y;
}