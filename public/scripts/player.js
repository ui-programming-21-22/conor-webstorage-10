// KEYPRESS console.log(event.keycode)
document.addEventListener("keydown", keyDown, false);
document.addEventListener("keyup", keyUp, false);
let playerImg = new Image();
playerImg.src = "pikachu.png";
let left = false;
let right = false;
let up = false;
let down = false;
let first = true;


var counter = 0;
animationInterval = setInterval(function() { 
counter++;
}, 50); 

function resetAnimation()
{
	counter = 0;
}

const player = {
    speed: 5,
    size: { x: 80, y: 123 },
    position: { x: 0, y: 0 },
    health: 100,

    spritesheet: { sx: 0, sy: 0, sw: 0, sh: 0},
    frame: -1,

    update() {
        if (this.position.y >= canvas.height - this.size.y) this.position.y = canvas.height - this.size.y;
        if (this.position.y <= -120 + this.size.y) this.position.y = -120 + this.size.y;
        if (this.position.x >= canvas.width - this.size.x) this.position.x = canvas.width - this.size.x;
        if (this.position.x <= -90 + this.size.x) this.position.x = -90 + this.size.x;

        if (left) {
            player.position.x -= player.speed;
        }
        if (right) {
            player.position.x += player.speed;
        }
        if (up) {
            player.position.y -= player.speed;
        }
        if (down) {
            player.position.y += player.speed;
        }
    },

    animate() {
        var anim = 0;
        if (counter > 1) 
        {
            resetAnimation();
            anim = 1;
        }
        if (anim == 1)
        {
            if (down || first)
            {   
                if (first) first = false;
                if (this.frame >= 3) this.frame = -1; 
                this.frame += 1;
                this.spritesheet.sx = 16 + (64 * this.frame);
                this.spritesheet.sy = 24;
                this.spritesheet.sw = 32;
                this.spritesheet.sh = 36;
            }
            else if (left)
            {
                if (this.frame >= 3) this.frame = -1; 
                this.frame += 1;
                this.spritesheet.sx = 16 + (64 * this.frame);
                this.spritesheet.sy = 90;
                this.spritesheet.sw = 34;
                this.spritesheet.sh = 34;
            }
            else if (right)
            {   
                if (this.frame >= 3) this.frame = -1; 
                this.frame += 1;
                this.spritesheet.sx = 14 + (64 * this.frame);
                this.spritesheet.sy = 154;
                this.spritesheet.sw = 34;
                this.spritesheet.sh = 34;
            }
            else if (up)
            {
                if (this.frame >= 3) this.frame = -1; 
                this.frame += 1;
                this.spritesheet.sx = 18 + (64 * this.frame);
                this.spritesheet.sy = 220;
                this.spritesheet.sw = 30;
                this.spritesheet.sh = 32;
            }
            anim = 0;
        }
        
    }
}

player.position.x = canvas.width / 2;
player.position.y = canvas.height / 2;



manager.on('end', function(evt, data)
{
    up = false;
    down = false;
    left = false;
    right = false;
    console.log("end");
});

manager.on('added', function(ev,dt)
{
    console.log("added");
            dt.on('dir:up', function(evt, data)
            {
                console.log("up");
                up = true;
                down = false;
                left = false;
                right = false;
            });
            dt.on('dir:down', function(evt, data)
            {
                down = true;
                left = false;
                right = false;
                up = false;
                right = false;
            });
            dt.on('dir:left', function(evt, data)
            {
                left = true;
                up = false;
                down = false;
                right = false;
            });
            dt.on('dir:right', function(evt, data)
            {
                right = true;
                up = false;
                down = false;
                left = false;
            });
        });
            

  function buttonClick(btn)
  {
      if (btn == 'left')
      {
          // do stuff
          if (player.health >= 10) player.health -= 10;
      }
      else if (btn == 'right')
      {
          // do other stuff
          if (player.health <= 90) player.health += 10;
      }
  }

function keyDown(e) { // GET KEY DOWN
    if (e.key == "Right" || e.key == "ArrowRight" || e.key == "d") {
        right = true;
    }
    if (e.key == "Left" || e.key == "ArrowLeft" || e.key == "a") {
        left = true;
    }
    if (e.key == "Up" || e.key == "ArrowUp" || e.key == "w") {
        up = true;
    }
    if (e.key == "Down" || e.key == "ArrowDown" || e.key == "s") {
        down = true;
    }
}

function keyUp(e) { // GET KEY UP
    if (e.key == "Right" || e.key == "ArrowRight" || e.key == "d") {
        right = false;
    }
    if (e.key == "Left" || e.key == "ArrowLeft" || e.key == "a") {
        left = false;
    }
    if (e.key == "Up" || e.key == "ArrowUp" || e.key == "w") {
        up = false;
    }
    if (e.key == "Down" || e.key == "ArrowDown" || e.key == "s") {
        down = false;
    }
}