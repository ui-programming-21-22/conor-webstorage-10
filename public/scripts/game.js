// CONTEXT
window.onload = () => {
    'use strict';
  
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('./sw.js');
    }
  }
  

const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");

var manager = nipplejs.create({
    zone: document.getElementById('nipplediv'),
    color: 'gray'
});



function update() {
    

    player.update();
    player.animate();
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height); //CLEAR SCREEN
    drawImg("background.png", new Vector2(0,0), new Vector2(1913, 961));
    
    ctx.drawImage(playerImg, player.spritesheet.sx, player.spritesheet.sy, player.spritesheet.sw, player.spritesheet.sh, player.position.x, player.position.y, player.size.x, player.size.y);

    //UI health
    drawQuad(new Vector2(0,0), new Vector2(500, 0), new Vector2(500,50), new Vector2(0, 50), "gray", true);
    drawQuad(new Vector2(0,0), new Vector2(player.health * 5, 0), new Vector2(player.health * 5,50), new Vector2(0, 50), "red", true);
}

function GameLoop() {
    update();
    draw()
    window.requestAnimationFrame(GameLoop);
}

window.requestAnimationFrame(GameLoop);
