
// READS THE SPRITESHEET AS FOLLOWS
// DOWN, LEFT, RIGHT, UP
//
// ↓ ↓ ↓ ↓
// ← ← ← ←
// → → → →
// ↑ ↑ ↑ ↑

class Animation {

    #readyToAnimate;
    #first;
    #frame;
    #columns;
    #frameSize;
    #spritesheet = { sx: 0, sy: 0, sw: 0, sh: 0};

    constructor(columns = 4, frameSize = 64) {
        this.#readyToAnimate = false;
        this.#columns = columns;
        this.#frameSize = frameSize;
        this.#first = true;
        this.#frame = -1;
        this.#spritesheet.sw = frameSize;
        this.#spritesheet.sh = frameSize;
    }
    
    animationInterval = setInterval(function() { 
        this.#readyToAnimate = true;
    }, 50); 

    animate(currentDir) {
        if (this.#readyToAnimate)
        {
            this.#readyToAnimate = false;
            if (currentDir == "down" || this.#first)
            {   
                if (this.#first) first = false;
                if (this.#frame >= this.#columns) this.#frame = -1; 
                this.#frame += 1;
                this.#spritesheet.sx = this.#frameSize * this.frame;
                this.#spritesheet.sy = this.#frameSize * this.frame;
            }
            else if (currentDir == "left")
            {
                if (this.#frame >= this.#columns) this.#frame = -1; 
                this.#frame += 1;
                this.#spritesheet.sx = this.#frameSize * this.#frame;
                this.#spritesheet.sy = this.#frameSize * this.#frame;
            }
            else if (currentDir == "right")
            {   
                if (this.#frame >= this.#columns) this.#frame = -1; 
                this.#frame += 1;
                this.#spritesheet.sx = this.#frameSize * this.#frame;
                this.#spritesheet.sy = this.#frameSize * this.#frame;
            }
            else if (currentDir == "up")
            {
                if (this.#frame >= this.#columns) this.#frame = -1; 
                this.#frame += 1;
                this.#spritesheet.sx = this.#frameSize * this.#frame;
                this.#spritesheet.sy = this.#frameSize * this.#frame;
            }
        }
    }
}